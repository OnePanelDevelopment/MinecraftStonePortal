<?php
   $pageTitle = 'Home';
   include './assets/includes/header.php';
?>
<div class="container centered">
   <div class="row">
      <!-- You can remove the width option once you've added your own logo -->
      <img class="logo" width="75%" src="https://onepanel.org/projects/global-assets/svg/minecraft-logo.svg" alt="Minecraft Logo" />
   </div>
   <div class="row">
      <div class="status">
	   <p>There is currently <span id="wow" class="font-purple" >  <i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i>  </span> players online at <span style="cursor:pointer" class="font-purple copyip" data-clipboard-text="mc.hypixel.net">mc.hypixel.net</span></p>
	  </div>
   </div>
   <div class="row" id="icons">
      <div class="col-md-3 sectionHover">
	    <a href="#">
	     <img class="image-forum" alt="Forums" />
		 <h2 class="default-text">Forums</h2>
		</a>
	  </div>
	  <div class="col-md-3 sectionHover">
	    <a href="#">
	     <img class="image-store" alt="Store" />
		 <h2 class="default-text">Store</h2>
		</a>
	  </div>
	  <div class="col-md-3 sectionHover">
	    <a href="#">
	     <img class="image-staff" alt="Staff" />
		 <h2 class="default-text">Staff</h2>
		</a>
	  </div>
	  <div class="col-md-3 sectionHover">
	    <a href="#">
	     <img class="image-users" alt="Players" />
		 <h2 class="default-text">Players</h2>
		</a>
	  </div>
   </div>
   <div class="row">
     <!--
	 Do not remove the following as doing so will null and void this copy of the Minecraft Portal - Stone by OnePanel Development (Matthew R).
	 The icons have also kindly been donated by CraftTillDawn, removing and link backs to them will result in revoking the permission to use the icons.
	 -->
	 <div class="col-md-6">
	    <a href="https://onepanel.org/" target="_blank"class="onePanelDevelopment">OnePanel Development</a>
	 </div>
	 <div class="col-md-6">
	    <a href="https://craftilldawn.com/" target="_blank" class="craftTillDawn">Icons By CraftTillDawn</a>
	 </div>
   </div>
</div>
<?php
   include './assets/includes/footer.php';
?>